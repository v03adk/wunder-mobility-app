# README for project setup ##

Wunder Mobility programming test

You can check the app at http://wma.alimov.eu

### How do I get set up? ###
* clone repo to _some_directory_
* you need php (at least 7.2.5) with modules php-intl, php-mbstring, php-xml, php-json, php-iconv, php-ctype  
* you need apache2 or nginx web server ready and running. configuring Symfony can be tricky. check apache2/nginx sample configs below
* you need _composer_. Please run _composer install_ in _some_directory_. This will install all needed dependencies
* you need mysql server ready and running. You can run migrations by _php bin/console doctrine:migrations:migrate_ or just import the database schema included in the repo
* adjust _.env_ file in the root directory. or create _.env.local_ with overrides



#### apache2 sample.conf ####
```
<VirtualHost *:80>
    ServerName wma.alimov.eu
    ServerAlias wma.alimov.eu
    ServerAdmin webmaster@localhost
    DocumentRoot some_directory/public
    <Directory some_directory/public>
        AllowOverride All
        Order allow,deny
        Allow from All
    </Directory>
    ErrorLog ${APACHE_LOG_DIR}/wma-error.log
    CustomLog ${APACHE_LOG_DIR}/wma-access.log combined
</VirtualHost>
```

#### nginx sample.conf ####
```
server {
        listen 80;

        server_name wma.local;
        root "some_directory/public";
        index index.html index.htm index.php;
        charset utf-8;

        location / {
            try_files $uri /index.php?$query_string;
        }

        location ~ ^/index\.php(/|$) {
            try_files $uri /index.php =404;
            fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
            internal;
        }
        location ~ /\.ht {
           deny all;
        }
}
```