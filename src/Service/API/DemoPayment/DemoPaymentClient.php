<?php

namespace App\Service\API\DemoPayment;

use App\Entity\CustomerPaymentInformation;
use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DemoPaymentClient implements DemoPaymentClientInterface
{
    private const URL = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
    private $httpClient;

    public function __construct(HttpClientInterface $client)
    {
        $this->httpClient = $client;
    }

    public function postData(CustomerPaymentInformation $paymentInformation): ?string
    {
        $data = [
            'customerId' => $paymentInformation->getCustomer()->getId(),
            'owner' => $paymentInformation->getAccountOwner(),
            'iban' => $paymentInformation->getIban(),
        ];
        try {
            $response = $this->httpClient->request('POST', self::URL, [
                'json' => $data,
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);

            if ($response->getStatusCode() === 200) {
                return json_decode($response->getContent(), true)['paymentDataId'];
            }
        } catch (Exception $e) {
        }

        return null;
    }
}
