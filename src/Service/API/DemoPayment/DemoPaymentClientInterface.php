<?php

namespace App\Service\API\DemoPayment;

use App\Entity\CustomerPaymentInformation;

interface DemoPaymentClientInterface
{
    public function postData(CustomerPaymentInformation $paymentInformation): ?string;
}
