<?php

namespace App\Service\API\DemoPayment;

use App\Entity\CustomerPaymentInformation;

class DemoPayment
{
    private $client;

    public function __construct(DemoPaymentClientInterface $client)
    {
        $this->client = $client;
    }

    public function getPaymentDataId(CustomerPaymentInformation $paymentInformation): ?string
    {
        return $this->client->postData($paymentInformation);
    }
}
