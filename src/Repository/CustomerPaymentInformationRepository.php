<?php

namespace App\Repository;

use App\Entity\CustomerPaymentInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomerPaymentInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomerPaymentInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomerPaymentInformation[]    findAll()
 * @method CustomerPaymentInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomerPaymentInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomerPaymentInformation::class);
    }

    // /**
    //  * @return CustomerPaymentInformation[] Returns an array of CustomerPaymentInformation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomerPaymentInformation
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
