<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\CustomerAddress;
use App\Entity\CustomerPaymentInformation;
use App\Form\CustomerAddressType;
use App\Form\CustomerPaymentInformationType;
use App\Form\CustomerType;
use App\Service\API\DemoPayment\DemoPayment;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends AbstractController
{
    public function index(Request $request): Response
    {
        // try to get customer from the cookie
        $customer = $this->tryGetCustomer($request);
        if ($customer !== null) {
            return $this->goToRegistrationStep($customer);
        }

        $customer = new Customer();

        $form = $this->createForm(CustomerType::class, $customer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customerRepository = $this->getDoctrine()->getRepository(Customer::class);
            // check if customer exists, e.g customer returned to registration after 1 day
            $checkCustomer = $customerRepository->findOneBy(['firstName' => $customer->getFirstName(), 'lastName' => $customer->getLastName(), 'phone' => $customer->getPhone()]);
            if ($checkCustomer !== null) {
                return $this->goToRegistrationStep($checkCustomer);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $customer->setCode(sha1($customer->getFirstName() . $customer->getLastName() . $customer->getPhone()));
            $customer->setRegistrationStep('address');
            $entityManager->persist($customer);
            $entityManager->flush();

            return $this->goToRegistrationStep($customer);
        }

        return $this->render('registration/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function tryGetCustomer(Request $request): ?Customer
    {
        $customerRepository = $this->getDoctrine()->getRepository(Customer::class);

        return $customerRepository->findOneBy(['code' => $request->cookies->get('customer_code')]);
    }

    private function goToRegistrationStep(Customer $customer): Response
    {
        $response = new RedirectResponse($this->generateUrl('app_registration_' . $customer->getRegistrationStep()));
        $response->headers->setCookie(Cookie::create('customer_code', $customer->getCode(), time() + 60 * 60 * 24));

        return $response;
    }

    public function address(Request $request): Response
    {
        // try to get customer from the cookie
        $customer = $this->tryGetCustomer($request);
        if ($customer !== null && $customer->getRegistrationStep() !== 'address') {
            return $this->goToRegistrationStep($customer);
        }

        // somebody tries to fake customer_code in the cookies or access the method by direct link
        if ($customer === null) {
            return $this->goToIndexWithCookieClear();
        }

        $address = new CustomerAddress();

        $form = $this->createForm(CustomerAddressType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $address->setCustomer($customer);
            $entityManager->persist($address);
            $customer->setRegistrationStep('payment_information');
            $entityManager->persist($customer);
            $entityManager->flush();

            return $this->redirectToRoute('app_registration_payment_information');
        }

        return $this->render('registration/address.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    private function goToIndexWithCookieClear()
    {
        $response = new RedirectResponse($this->generateUrl('app_registration_index'));
        $response->headers->clearCookie('customer_code');

        return $response;
    }

    public function paymentInformation(Request $request, DemoPayment $demoPayment): Response
    {
        // try to get customer from the cookie
        $customer = $this->tryGetCustomer($request);
        if ($customer !== null && $customer->getRegistrationStep() !== 'payment_information') {
            return $this->goToRegistrationStep($customer);
        }

        // somebody tries to fake customer_code in the cookies or access the method by direct link
        if ($customer === null) {
            return $this->goToIndexWithCookieClear();
        }

        if ($customer->getCustomerPaymentInformations()->count() > 0) {
            $paymentInformation = $customer->getCustomerPaymentInformations()->get(0);
        } else {
            $paymentInformation = new CustomerPaymentInformation();
        }

        $form = $this->createForm(CustomerPaymentInformationType::class, $paymentInformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentInformation->setCustomer($customer);
            $paymentInformation->setPaymentDataId($demoPayment->getPaymentDataId($paymentInformation));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($paymentInformation);
            $entityManager->flush();

            if ($paymentInformation->getPaymentDataId()) {
                $customer->setRegistrationStep('success_page');
                $entityManager->persist($customer);
                $entityManager->flush();

                return $this->redirectToRoute('app_registration_success_page');
            }

            $this->addFlash(
                'danger',
                'paymentDataId wasn\'t received from the remote API. please hit Save button again'
            );
            return $this->redirectToRoute('app_registration_payment_information');
        }

        return $this->render('registration/payment-information.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function successPage(Request $request): Response
    {
        // try to get customer from the cookie
        $customer = $this->tryGetCustomer($request);
        if ($customer !== null && $customer->getRegistrationStep() !== 'success_page') {
            return $this->goToRegistrationStep($customer);
        }

        // somebody tries to fake customer_code in the cookies or access the method by direct link
        if ($customer === null) {
            return $this->goToIndexWithCookieClear();
        }

        return $this->render('registration/success-page.html.twig', [
            'customer' => $customer,
        ]);
    }
}
