<?php

namespace App\Entity;

use App\Repository\CustomerPaymentInformationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerPaymentInformationRepository::class)
 */
class CustomerPaymentInformation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $iban;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $paymentDataId;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="customerPaymentInformations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $accountOwner;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getPaymentDataId(): ?string
    {
        return $this->paymentDataId;
    }

    public function setPaymentDataId(string $paymentDataId): self
    {
        $this->paymentDataId = $paymentDataId;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getAccountOwner(): ?string
    {
        return $this->accountOwner;
    }

    public function setAccountOwner(string $accountOwner): self
    {
        $this->accountOwner = $accountOwner;

        return $this;
    }
}
