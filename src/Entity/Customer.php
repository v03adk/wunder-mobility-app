<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity=CustomerAddress::class, mappedBy="customer", orphanRemoval=true)
     */
    private $customerAddresses;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $code;

    /**
     * @ORM\OneToMany(targetEntity=CustomerPaymentInformation::class, mappedBy="customer", orphanRemoval=true)
     */
    private $customerPaymentInformations;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $registrationStep;

    public function __construct()
    {
        $this->customerAddresses = new ArrayCollection();
        $this->customerPaymentInformations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|CustomerAddress[]
     */
    public function getCustomerAddresses(): Collection
    {
        return $this->customerAddresses;
    }

    public function addCustomerAddress(CustomerAddress $customerAddress): self
    {
        if (!$this->customerAddresses->contains($customerAddress)) {
            $this->customerAddresses[] = $customerAddress;
            $customerAddress->setCustomer($this);
        }

        return $this;
    }

    public function removeCustomerAddress(CustomerAddress $customerAddress): self
    {
        if ($this->customerAddresses->removeElement($customerAddress)) {
            // set the owning side to null (unless already changed)
            if ($customerAddress->getCustomer() === $this) {
                $customerAddress->setCustomer(null);
            }
        }

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|CustomerPaymentInformation[]
     */
    public function getCustomerPaymentInformations(): Collection
    {
        return $this->customerPaymentInformations;
    }

    public function addCustomerPaymentInformation(CustomerPaymentInformation $customerPaymentInformation): self
    {
        if (!$this->customerPaymentInformations->contains($customerPaymentInformation)) {
            $this->customerPaymentInformations[] = $customerPaymentInformation;
            $customerPaymentInformation->setCustomer($this);
        }

        return $this;
    }

    public function removeCustomerPaymentInformation(CustomerPaymentInformation $customerPaymentInformation): self
    {
        if ($this->customerPaymentInformations->removeElement($customerPaymentInformation)) {
            // set the owning side to null (unless already changed)
            if ($customerPaymentInformation->getCustomer() === $this) {
                $customerPaymentInformation->setCustomer(null);
            }
        }

        return $this;
    }

    public function getRegistrationStep(): ?string
    {
        return $this->registrationStep;
    }

    public function setRegistrationStep(string $registrationStep): self
    {
        $this->registrationStep = $registrationStep;

        return $this;
    }
}
