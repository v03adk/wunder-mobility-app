README for answering questions

Wunder Mobility programming test

You can check the app at http://wma.alimov.eu


1. Describe possible performance optimizations for your Code.

Not sure about performance optimizations for the code, such a small app and simple workflow
but
* if table grow bigger I will create indexes on customer_code in customer table and customer_id in customer_address/customer_payment_information tables
* I think all other concerns not about performance

2. Which things could be done better, than you’ve done it?
 I would rephrase this question to "What are the next steps?"
 * adding functional tests
 * refactoring of the controller, it's definitely not 'thin'
 * changing UI, make 1 page with steps changing with ajax requests to the backend





